#!/usr/local/bin/python
from time import strftime
import sys

# bug : ./mytests.py special=3 : repetition de 'special' 2 fois dans LoK !
######################################################################
def main(special='0.0',coucou='false'):
    coucoubool = (coucou=='true' or coucou=='True' or coucou=='TRUE' or coucou=='1' or coucou=='')
    
    specialf = 0
    try :
        specialf = float(special)
        if special.isalpha():return False
    except ValueError :
        sys.stderr.write("!\tError in main : special must be input with a float !\n")
        sys.stderr.write("!\tFor exemple \t:\n")
        sys.stderr.write("!\t\t\t>   mytest.py special=3.1415\n")
        sys.stderr.write("!\t\tor\t>   mytest.py special='3.1415'\n\n")
    print("\t\tmy special\t: %s" % specialf)
    print("\t\tcoucou\t\t: "+str(coucoubool))
    return 0
######################################################################

def myfct():
    sys.exit('>> EoP : Good Execution of  \'myfct\' <<')


######################################################################
## Execution du script suivant les differents cas de figure ##########
######################################################################
def readKeywords():
    print(">> Start readKeywords")
    nbArgv = len(sys.argv)
    LoK = []                                                      # List of Keywords
    LoP = []                                                      # List of Parameters
    if nbArgv == 1 : keywordIsPresent=False
    if nbArgv > 1 :
        keywordIsPresent=True
        i = 1
        while i < nbArgv :
            LoK.append(sys.argv[i].split('=')[0])
            
            if len(sys.argv[i].split('=')) == 1:
                LoP.append('')
            else:
                LoP.append(sys.argv[i].split('=')[1])
            #EndIf
            
            sys.stdout.write(('\tArgument {0}/{1} :\n'+\
                              '\t\tkeyword\t\t: {2}\n'+\
                              '\t\tparameter\t: {3}\n')\
                             .format(\
                                     str(i),\
                                     str(nbArgv-1),\
                                     LoK[i-1],\
                                     LoP[i-1]\
                             ))
            i = i + 1
        #Endwhile
    #EndIf
    print(">> End   readKeywords") #IcI ca foncitonne !
    return keywordIsPresent, LoK, LoP

#####################################################################
def casesKeywords(keywordIsPresent, LoK, LoP,
                  ListKeyword= ['special', 'coucou'],\
                  ListKwFct = ['myfct']\
                  ):
    print(">> Start casesKeywords")
    nbArg = len(LoK)
    keywordIsInList = False

    #Commencons par regarder si l un des mot-cle n est pas dans la liste
    #si on tombe sur un mot-cle n ayant pas besoin du main :
    for i in range(0, nbArg):
        for j in range(0, len(ListKwFct)):                        # si le mot-clef appelle une fct
            if LoK[i] == ListKwFct[j]:
                keywordInList = True
                globals()[ListKwFct[j]]() #ToDo : ajouter possibilitee d ecrire des mots-clefs
                sys.exit('>> ERROR : EoP > the function did not exit by itself <<%s'%str(strftime('%C')))
            #EndIf
        #EndFor
    #EndFor

    # En principe, comme on a traite les cas ou le main n est pas execute avant,
    # il n'y aura pas de probleme ici : seul les mots-cle 
    LoKInList = []
    LoPInList = []
    for i in range(0, nbArg):
        for j in range(0, len(ListKeyword)):
            keywordIsInList = (keywordIsInList or LoK[i] == ListKeyword[j])
            LoKInList.append(LoK[i])   # Ne gardons que les mots clef existant
            LoPInList.append(LoP[i])
            #print(i,j)
        #EndFor
    #EndFor

    #ToDo : supprimer doublons !
    
    #print(LoKInList)
    #print(LoK)
    
    if len(LoKInList) != len(LoK) :
        sys.stderr.write('List of keywords : '+str(ListKwFct)+str(ListKeyword)+'\n')
        sys.exit('>> ERROR : EoP > Bad keyword used ! <<%s'%str(strftime('%C'))) 
    main(**{LoKInList[i]:LoPInList[i] for i in range(0,len(LoKInList))}) # On aura deja quitte dans les cas ou ca ne fonctionne pas ou dans les cas ou il n y a pas d arguments
    print(">> End   casesKeywords")

##############################"

KIP, LoK, LoP = readKeywords()
print("")
casesKeywords(KIP, LoK, LoP)
