import os
import sys

# TODO :
# faire un main de tests unitaires
# (avec creations de dossiers tmp et leurs suppression ensuite)


def listFilesInDir(
        thePath=None,
        absolutePath=True,
        alsoDir=False,
        onlyDir=False
):
    # ####################################################################### #
    # CC By Pierre Maltey                                                2020 #
    # Input :                                                                 #
    #       - thePath      (opt) : relative or absolute path                  #
    #       - absolutePath (opt) : return the absolute path                   #
    #       - alsoDir      (opt) : return files and directories in the dir    #
    #       - onlyDir      (opt) : return sub-directories in the dir          #
    # Output :                                                                #
    #       - list of files and/or subdirectories in the directory            #
    # ####################################################################### #

    if thePath in ["", ".", "./"]:
        thePath = None
    elif  "~" in thePath:
        raise RuntimeError(
            "ERROR (listFilesInDir) : I can not manage TILD as path\n"
        )
    # endIf

    if absolutePath:
        try:
            thePath = os.path.abspath("./" if thePath is None else thePath)
        except Exception as e:
            sys.stderr.write(e)
            sys.exit(31)
        # endTry
    # endIf

    try:
        allEltsInDir = os.listdir(thePath)
    except Exception as e:
        sys.stderr.write(e)
        sys.exit(32)
    # endTry

    try:
        if thePath is None:

            if alsoDir:
                return allEltsInDir
            elif onlyDir:
                return list(
                    filter(
                        lambda eachFile:
                        os.path.isdir(eachFile),
                        allEltsInDir
                    )
                )
            else:
                return list(
                    filter(
                        lambda eachFile:
                        not os.path.isdir(eachFile),
                        allEltsInDir
                    )
                )
            # endIf

        else: # thePath is not None

            if alsoDir:
                return [
                    os.path.join(thePath, ef)
                    for ef in allEltsInDir
                ]
            elif onlyDir:
                return [
                    os.path.join(thePath, ef) for ef in
                    filter(
                        lambda eachFile:
                        os.path.isdir(os.path.join(thePath, eachFile)),
                        allEltsInDir
                    )
                ]
            else:
                return [
                    os.path.join(thePath, ef) for ef in 
                    filter(
                        lambda eachFile:
                        not os.path.isdir(os.path.join(thePath, eachFile)),
                        allEltsInDir
                    )
                ]
            # endIf
            
        # endIf

    except Exception as e:
        sys.stderr.write(e)
        sys.stderr.write(
            "!!! UNEXPECTED ERROR (function: listFilesInDir)!!!\n"
        )
        sys.exit(33)
    # endTry

    return []


def fileExists(
    theFile,
    alsoDir=False,
    onlyDir=False
):
    # ####################################################################### #
    # CC By Pierre Maltey                                                2020 #
    # Input :                                                                 #
    #       - theFile            : the file to test                           #
    #       - alsoDir      (opt) : theFile can be a file or a dir             #
    #       - onlyDir      (opt) : theFile  must be a dir                     #
    # Output :                                                                #
    #       - return True if theFile exists                                   #
    # ####################################################################### #
    if alsoDir: 
        try:
            return (os.path.isfile(theFile) or os.path.isdir(theFile))
        except Exception as e:
            sys.stderr.write(e)
            sys.exit(35)
        # endTry
    elif onlyDir: 
        try:
            return os.path.isdir(theFile)
        except Exception as e:
            sys.stderr.write(e)
            sys.exit(36)
        # endTry
    else:
        try:
            return os.path.isfile(theFile)
        except Exception as e:
            sys.stderr.write(e)
            sys.exit(35)
        # endTry
    sys.exit(36)


def findFile(
        theFile,
        thePath=None,
        theExtension=None,
        boolCheckExist=True,
        absolutePath=False,
        verbose=False,
        verboseAll=False
        ):
    # ####################################################################### #
    # CC By Pierre Maltey                                         2019 - 2020 #
    # Input :                                                                 #
    #       - theFile            : filename                                   #
    #       - thePath      (opt) : relative or absolute path                  #
    #       - theExtension (opt)                                              #
    #       - absolutePath (opt) : return the absolute path                   #
    #       - verbose      (opt)                                              #
    # Output :                                                                #
    #       - myFile : the file with path and extension                       #
    # ####################################################################### #

    if thePath is None:  # Set the default path
        thePath = "./"
    # endIf

    if verboseAll:      # Set the verbose option if verboseAll
        verbose = True
    # endIf

    if theExtension is not None:  # Clean the extension if necessary
        if (theExtension[0] == "."):
            if (len(theExtension) > 1):
                theExtension = TheExtension[1:]
            elif (len(theExtension) == 1):
                sys.stderr.write(
                    r"Warning (findFile) : " +
                    r"bad theExtension entry : " +
                    r"I'll remove it !\n"
                )
                theExtension = None
            # endIf
        # endIf (start with dot)
    # endIf

    if(
            (theExtension is not None) and
            (theFile.split(".")[-1] != theExtension)
    ):
        theFile = '{}.{}'.format(theFile, theExtension)

        if verboseAll:
            sys.stdout.write(
                ">> I add the extension {} to the filename {}\n".format(
                    theExtension,
                    theFile
                )
            )
        # endIf
    # endIf

    if "/" in theFile:
        pathtmp = theFile.split("/")
        thePath = os.path.join(thePath, '/'.join(pathtmp[:-1]))
        theFile = pathtmp[-1]
    # endIf

    if thePath == "./":
        myFile = theFile
    else:
        myFile = os.path.join(thePath, theFile)
    # endIf

    if verboseAll:
        sys.stdout.write(
            ">> I'll search {} in the directory {}\n".format(
                theFile,
                thePath
            )
        )
    # endIf

    if absolutePath:

        if verboseAll:
            sys.stdout.write(
                ">> I'll return the absolute path of the file:\n    {}\n".format(
                    myFile
                )
            )
        # endIf

        myFile = os.path.abspath(myFile)

    else:

        if verboseAll:
            sys.stdout.write(
                ">> I'll return the relative path of the file\n    {}\n".format(
                    myFile
                )
            )
        # endIf

    # endIf

    if fileExists(myFile):

        if verbose:
            sys.stdout.write(">> I found the file : {}\n".format(myFile))
        # endIf

    elif boolCheckExist:

        raise RuntimeError(
            "I have not found the file " +
            "(and I can't continue due to boolCheckExist parameter in findFile):" +
            "\n    {}\n".format(myFile)
        )

    else:

        sys.stderr.write(
            "!! I have not found the file, but I'll return it anyway\n    : {}\n".format(
                myFile
            )
        )

    # endIf

    return myFile


def readFile(theFile, dataType=str, separator=None, commentSymb="#"):
    # ####################################################################### #
    # CC By Pierre Maltey                                               2019  #
    # Input :                                                                 #
    #       - theFile : the file with path and extension                      #
    #       - dataType (opt, def = str) : type to read (type)                 #
    #       - separator (opt, def=None) : separator between columns(None, str)#
    #       - commentSymb (opt, def="#"): symbol for comment inline (str)     #
    # Output :                                                                #
    #       - CoF : table   : Content of File                                 #
    #                         empty columns is replaced by the bool  : False  #
    # ####################################################################### #

    try:                                              # Check if the file exist
        findFile(theFile, boolFullPath=True)
    except:
        return []

    with open(theFile, 'r') as myFile:                         # Read the file
        CoF = myFile.readlines()
        myFile.close()                                        # Close the file
    # EndWidth

    dataTable = []
    boolAlert = False

    for elt in [(x.split('\n')[0]).split(commentSymb)[0] for x in CoF]:
        elt = elt.split(separator)

        boolAlert = boolAlert or ((separator != ";") and ";" in elt[0])
        try:
            dataTable.append(
                [dataType(x) if x else False for x in elt]
                if len(elt) > 1 else
                dataType(elt[0])
            )
        except ValueError:
            pass
        # EndTry
    # EndFor

    if boolAlert:
        sys.stderr.write("*-WARNING-* (readFile)")
        sys.stderr.write(
            "separator option is not found but I detected a \";\""
            "or a \",\" in one line or more !"
            "\n\t\t\t"
            "Maybe you are using the default option (separator=None)"
            "\n"
        )
    # EndIf

    return dataTable